﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class LevelPrefabsPool
{
    public Dictionary<string, GameObject> Objects = new Dictionary<string, GameObject>();
    
    public IEnumerator Load(List<string> names, Action<float> SetProgress)
    {
        Objects.Clear();
        for (int i=0; i<names.Count; i++)
        {
            var name = names[i];
            Objects.Add(name, (GameObject)Resources.Load(name));
            SetProgress?.Invoke((float)(i+1) / names.Count);
            yield return new WaitForSeconds(1f);
        }
    }
}
