﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuScreen : MonoBehaviour
{
    public event EventHandler GameplayClicked;
    
    [SerializeField]
    private Button _gameplayButton;
    [SerializeField]
    private Button _settingsButton;
    [SerializeField]
    private Button _storeButton;
    [SerializeField]
    private Button _cheatsButton;
    
    private void Awake()
    {
        _gameplayButton.onClick.AddListener(() => GameplayClicked?.Invoke(this, EventArgs.Empty));
    }
}
