﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TownScreen : MonoBehaviour
{
    public event EventHandler ExitClicked;
    
    [SerializeField]
    private Button _exitButton;

    private void Awake()
    {
        _exitButton.onClick.AddListener(() => ExitClicked?.Invoke(this, EventArgs.Empty));
    }
}
