﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour
{
    [SerializeField]
    private Image _bar;
    [SerializeField]
    private Text _label;
    
    public void SetPercent(int percent)
    {
        _bar.fillAmount = percent / 100f;
        _label.text = string.Format("{0}%", percent);
    }
}
