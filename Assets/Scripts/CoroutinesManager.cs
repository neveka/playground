﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutinesManager : MonoBehaviour
{
    public void StartRoutine(IEnumerator routine)
    {
        StartCoroutine(routine);
    }
}
