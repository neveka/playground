﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownController : ScreenController<TownScreen>
{
    private GameObject _treeObjectPrefab;
    private List<GameObject> _treeObjects = new List<GameObject>();
    private GridView _gridView;
    
    private readonly LevelPrefabsPool _prefabsPool;
    
    public TownController(UIManager uiManager, LevelPrefabsPool prefabsPool) : base(uiManager, ScreenType.Town)
    {
        _prefabsPool = prefabsPool;
    }

    protected override void InitScreen()
    {
        if (_treeObjectPrefab == null)
        {
            _treeObjectPrefab = _prefabsPool.Objects["Tree"];
        }

        if (_gridView == null)
        {
            var gridPrefab = _prefabsPool.Objects["Grid"];
            _gridView = GameObject.Instantiate(gridPrefab).GetComponent<GridView>();
        }

        _screenObject.ExitClicked += OnExitClicked;
        _gridView.SlotClicked += OnSlotClicked;
    }

    private void OnSlotClicked(object sender, Vector3 position)
    {
        var tree = GameObject.Instantiate(_treeObjectPrefab, position, Quaternion.identity);
        _treeObjects.Add(tree);
    }

    protected override void BeforeClose()
    {
        foreach (var tree in _treeObjects)
        {
            GameObject.Destroy(tree);
        }
        
        _screenObject.ExitClicked -= OnExitClicked;
        _gridView.SlotClicked -= OnSlotClicked;
    }

    private void OnExitClicked(object sender, EventArgs e)
    {
        _uiManager.CloseScreen(_screenType);
        _uiManager.OpenScreen(ScreenType.MainMenu);
    }
}
