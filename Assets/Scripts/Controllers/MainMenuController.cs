﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : ScreenController<MainMenuScreen>
{
    public MainMenuController(UIManager uiManager) : base(uiManager, ScreenType.MainMenu)
    {
    }

    protected override void InitScreen()
    {
        _screenObject.GameplayClicked += OnGameplayClicked;
    }

    protected override void BeforeClose()
    {
        _screenObject.GameplayClicked -= OnGameplayClicked;
    }

    private void OnGameplayClicked(object sender, EventArgs e)
    {
        _uiManager.CloseScreen(_screenType);
        _uiManager.OpenScreen(ScreenType.LevelLoading);
    }
}
