﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Object = System.Object;

public class ScreenController<T> : IDisposable where T : UnityEngine.Object
{
    protected readonly UIManager _uiManager;
    protected readonly ScreenType _screenType;
    protected T _screenObject;
    
    public ScreenController(UIManager uiManager, ScreenType screenType)
    {
        _uiManager = uiManager;
        _screenType = screenType;
        _uiManager.ScreenOpened += OnScreenOpened;
        _uiManager.ScreenClosed += OnScreenClosed;
    }

    protected virtual void InitScreen() {}
    
    protected virtual void BeforeClose() {}

    protected virtual void OnScreenOpened(object sender, ScreenType screenType)
    {
        if(screenType != _screenType || _screenObject != null) return;

        var path = "Screens/" + typeof(T);
        var prefab = Resources.Load<T>(path);
        if (prefab == null)
        {
            Debug.Log("Cannot load prefab "+path);
        }
        _screenObject = GameObject.Instantiate(prefab);

        InitScreen();
    }
    
    protected virtual void OnScreenClosed(object sender, ScreenType screenType)
    {
        if(screenType != _screenType || _screenObject == null) return;

        BeforeClose();
        GameObject.Destroy((_screenObject as MonoBehaviour).gameObject);
        //_screenObject = null;
    }

    public void Dispose()
    {
        _uiManager.ScreenOpened -= OnScreenOpened;
        _uiManager.ScreenClosed -= OnScreenClosed;
    }
}
