﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class StartLoadingController : ScreenController<LoadingScreen>
{
    private float _loadingDuration = 3f;

    public StartLoadingController(UIManager uiManager, CoroutinesManager coroutinesManager) : base(uiManager, ScreenType.StartLoading)
    {
        coroutinesManager.StartRoutine(FakeLoading());
    }

    private IEnumerator FakeLoading()
    {
        var startLoadingTime = Time.time;
        _uiManager.OpenScreen(_screenType);
        while (Time.time - startLoadingTime < _loadingDuration)
        {
            _screenObject.SetPercent((int) ((Time.time - startLoadingTime) * 100 / _loadingDuration));
            yield return null;
        }

        _uiManager.CloseScreen(_screenType);
        _uiManager.OpenScreen(ScreenType.MainMenu);
    }
}
