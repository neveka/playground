﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLoadingController : ScreenController<LoadingScreen>
{
    private readonly LevelPrefabsPool _prefabsPool;
    private readonly CoroutinesManager _coroutinesManager;
    private readonly List<string> _prefabs = new List<string> { "Grid", "Tree"};

    public LevelLoadingController(UIManager uiManager, LevelPrefabsPool prefabsPool,
        CoroutinesManager coroutinesManager) : base(uiManager, ScreenType.LevelLoading)
    {
        _prefabsPool = prefabsPool;
        _coroutinesManager = coroutinesManager;
    }

    protected override void InitScreen()
    {
        _coroutinesManager.StartCoroutine(Loading());
    }

    private IEnumerator Loading()
    {
        yield return _coroutinesManager.StartCoroutine(_prefabsPool.Load(_prefabs, SetProgress));
        _uiManager.CloseScreen(_screenType);
        _uiManager.OpenScreen(ScreenType.Town);
    }

    private void SetProgress(float progress)
    {
        _screenObject.SetPercent((int)(progress * 100));
    }
}
