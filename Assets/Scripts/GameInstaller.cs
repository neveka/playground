using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<CoroutinesManager>().FromComponentInHierarchy().AsSingle();
        Container.Bind<UIManager>().AsSingle();
        Container.Bind<LevelPrefabsPool>().AsSingle();
        
        Container.Bind<LevelLoadingController>().AsSingle().NonLazy();
        Container.Bind<MainMenuController>().AsSingle().NonLazy();
        Container.Bind<TownController>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<StartLoadingController>().AsSingle().NonLazy();
    }
}