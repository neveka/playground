﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridView : MonoBehaviour
{
    public event EventHandler<Vector3> SlotClicked;
    
    private List<Button> _slots = new List<Button>();

    private void Awake()
    {
        _slots.AddRange(GetComponentsInChildren<Button>());
        foreach (var slot in _slots)
        {
            slot.onClick.AddListener( ()=> SlotClicked?.Invoke(this, slot.transform.position));
        }
    }
}
