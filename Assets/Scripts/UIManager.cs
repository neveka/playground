﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ScreenType
{
    StartLoading,
    MainMenu,
    LevelLoading,
    Town
}

public class UIManager
{

    
    public event EventHandler<ScreenType> ScreenOpened;
    public event EventHandler<ScreenType> ScreenClosed;

    private HashSet<ScreenType> _openScreens = new HashSet<ScreenType>();
    
    public void OpenScreen(ScreenType screenType)
    {
        if(_openScreens.Contains(screenType)) return;
        
        ScreenOpened?.Invoke(this, screenType);
        _openScreens.Add(screenType);
    }

    public void CloseScreen(ScreenType screenType)
    {
        if(!_openScreens.Contains(screenType)) return;
        
        ScreenClosed?.Invoke(this, screenType);
        _openScreens.Remove(screenType);
    }
}
